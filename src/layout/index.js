import React from 'react';

import Section from 'grommet/components/Section';
import Tabs from 'grommet/components/Tabs';
import Tab from 'grommet/components/Tab';

export const AppLayout = () => (
  <Section>
    <Tabs>
      <Tab title='Incoming' />
      <Tab title='Assigned' />
      <Tab title='Delivered' />
    </Tabs>
  </Section>
);

export default AppLayout;
