import React from 'react';

import Header from 'grommet/components/Header';
import Title from 'grommet/components/Title';

export const AppHeader = () => (
  <Header>
    <Title>
      Central Dispatch
    </Title>
  </Header>
);

export default AppHeader;
