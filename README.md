# Central Dispatch

Admin central-dispatch module for scheduling deliveries.

Instructions:

  1. Install dependencies
     ```
     $ yarn install
     ```
  2. Test and run linters
     ```
     $ yarn test
     ```
  3. Start app in development mode
     ```
     $ yarn start
     ```
  4. Pack app for production
     ```
     $ yarn build
     ```
