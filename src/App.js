import React, { Component } from 'react';

import GrommetApp from 'grommet/components/App'; // App
import Article from 'grommet/components/Article';

import AppHeader from './header';
import AppFooter from './footer';
import AppLayout from './layout';
// import socket from './phoenixSocket';

class App extends Component {
  render() {
    return (
      <GrommetApp>
        <Article>
          <AppHeader />
          <AppLayout />
          <AppFooter />
        </Article>
      </GrommetApp>
    );
  }
}

export default App;
