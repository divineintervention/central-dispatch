import {combineReducers} from 'redux';

import couriers from './couriers/reducer';

export default combineReducers({
  couriers
});
