export const createReducer = (initialState, handlers) =>
  (state = initialState, action) => {
    const handler = handlers[action.type];
    return (handler === undefined) ? // only when its undefined
      state :
      {...state, ...handler(state, action)};
  };

export default {createReducer};
